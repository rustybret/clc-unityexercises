﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour {

    const float degressPerHour = 30.0f;
    const float degreesPerMinute = 6f;
    const float degreesPerSecond = 6f;
    public bool continuous;

    public Transform hoursTransform, minutesTransform, secondsTransform;

    void Update() {
        if (continuous)
        {
            UpdateContinuous();
        }
        else
        {
            UpdateDiscrete();
        }
    } 

    void UpdateContinuous() {
        DateTime now = DateTime.Now;
        TimeSpan time = DateTime.Now.TimeOfDay;
        //Debug.Log("Log Awake on Clock at " + now.Hour);

        hoursTransform.localRotation = Quaternion.Euler(0.0f, (float)time.TotalHours * degressPerHour, 0.0f);
        minutesTransform.localRotation = Quaternion.Euler(0f, (float)time.TotalMinutes * degreesPerMinute, 0f);
        secondsTransform.localRotation = Quaternion.Euler(0f, (float)time.TotalSeconds * degreesPerSecond, 0f);

       // Debug.Log("UpdateContinuous setting hours hand to " + now.Hour * degressPerHour);      
    }
    void UpdateDiscrete()
    {
        DateTime time = DateTime.Now;
        DateTime now = DateTime.Now;
        //Debug.Log("Log Awake on Clock at " + now.Hour);

        hoursTransform.localRotation = Quaternion.Euler(0.0f, now.Hour * degressPerHour, 0.0f);
        minutesTransform.localRotation = Quaternion.Euler(0f, now.Minute * degreesPerMinute, 0f);
        secondsTransform.localRotation = Quaternion.Euler(0f, now.Second * degreesPerSecond, 0f);

        //Debug.Log("UpdateDiscrete setting hours hand to " + now.Hour * degressPerHour);
    }
}
