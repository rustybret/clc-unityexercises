﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum GraphFunctionName  {Sine, Sine2D, Multisine, CrazyMultiSine, MultiSine2D, Ripple, Cylinder, Sphere}
